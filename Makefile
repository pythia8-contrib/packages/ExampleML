# Makefile is a part of the PYTHIA-CONTRIB package ExampleML.
# Copyright (C) 2023 AUTHORS.
# ExampleML is licenced under the GNU GPL v2 or later, see COPYING for details.
# Please respect the MCnet Guidelines, see GUIDELINES for details.

# This is is the Makefile used to build ExampleML on POSIX systems.
# Example usage is:
#     make -j2
# For help using the make command please consult the local system documentation,
# i.e. "man make" or "make --help".

################################################################################
# VARIABLES: Definition of the relevant variables from the configuration script.
################################################################################

# Set the shell.
SHELL=/usr/bin/env bash

# Include the ExampleML configuration.
-include Makefile.inc

# Local directory structure.
LOCAL_INCLUDE=include
LOCAL_SRC=src
LOCAL_TMP=tmp
LOCAL_LIB=lib
LOCAL_MKDIRS:=$(shell mkdir -p $(LOCAL_TMP) $(LOCAL_LIB))

# ExampleML.
OBJECTS=$(patsubst $(LOCAL_SRC)/%.cc,$(LOCAL_TMP)/%.o,\
	$(sort $(wildcard $(LOCAL_SRC)/*.cc)))
TARGETS=$(LOCAL_LIB)/libExampleML.so

# Compiler flags.
CXX_SHARED+= -Wl,-undefined,dynamic_lookup
CXX_COMMON:=-I$(LOCAL_INCLUDE) $(PYTHIA8_INCLUDE) $(CXX_COMMON)
OBJ_COMMON:=-MD $(CXX_COMMON) $(OBJ_COMMON)

################################################################################
# RULES: Definition of the rules used to build ExampleML.
################################################################################

# Rules without physical targets (secondary expansion for specific rules).
.SECONDEXPANSION:
.PHONY: all contrib clean

# All targets.
all: $(TARGETS)

# Copy ExampleML to PYTHIA-CONTRIB after building.
contrib: $(LOCAL_LIB)/libExampleML.so
	cp $< ../lib/
	cp -r include/ExampleML ../include/Pythia8Contrib/
	cp -r share/ExampleML ../share/Pythia8Contrib/

# Clean.
clean:
	rm -rf $(LOCAL_TMP) $(LOCAL_LIB)

# The Makefile configuration.
Makefile.inc:
	./configure

# Auto-generated (with -MD flag) dependencies.
-include $(LOCAL_TMP)/*.d

# ExampleML.
$(LOCAL_TMP)/%.o: $(LOCAL_SRC)/%.cc
	$(CXX) $< -o $@ -c $(OBJ_COMMON) $(ORT_INCLUDE)
$(LOCAL_LIB)/libExampleML.so: $(OBJECTS)
	$(CXX) $^ -o $@ $(CXX_COMMON) $(CXX_SHARED) $(CXX_SONAME)$(notdir $@)\
	 $(ORT_LIB)
