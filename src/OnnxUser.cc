// OnnxUser.cc is a part of the PYTHIA-CONTRIB package ExampleML.
// Copyright (C) 2023 AUTHORS.
// ExampleML is licenced under the GNU GPL v2 or later, see COPYING for details.
// Please respect the MCnet Guidelines, see GUIDELINES for details.

// Function definitions (not found in the header) for the OnnxUser class.

#include "ExampleML/OnnxUser.h"

namespace Pythia8 {

namespace Pythia8Contrib {

namespace ExampleML {

//==========================================================================

// The OnnxUser class.

//--------------------------------------------------------------------------

// Register the plugin class so that it can be loaded by Pythia. The
// final three arguments specify whether the Pythia pointer, Settings
// pointer, or Logger pointer are required to be valid pointers when
// the class is constructed. Change these accordingly to false if
// these pointers are not required.

PYTHIA8_PLUGIN_CLASS(UserHooks, OnnxUser,
  true, // Require the Pythia pointer to be valid.
  true, // Require the Settings pointer to be valid.
  true) // Require the Logger pointer to be valid.

//--------------------------------------------------------------------------

// Constructor.

OnnxUser::OnnxUser(
  Pythia* pythiaPtrIn, Settings* settingsPtrIn,
  Logger* loggerPtrIn) : UserHooks() {

  // This loads the flags deciding whether to run the model and the model address
  canChangeFragParFlag = settingsPtrIn->flag("OnnxUser:canChangeFragPar");
  hadronizationNN = settingsPtrIn->word("OnnxUser:hadronizationNN");
  maxWeight = settingsPtrIn->parm("OnnxUser:maxWeight");

  // Create the ORT environment.
  Ort::Env env(OrtLoggingLevel::ORT_LOGGING_LEVEL_WARNING, "OnnxUser");

  // Create the session options (TO-DO pass with XML).
  Ort::SessionOptions options;
  options.SetGraphOptimizationLevel(GraphOptimizationLevel::ORT_ENABLE_ALL);
  
  // Create the ORT session (TO-DO pass with XML).
  session = make_shared<Ort::Session>(env, hadronizationNN.c_str(),options);

  // Create ORT allocator.
  Ort::AllocatorWithDefaultOptions allocator;
  
  // Get metadata.
  Ort::ModelMetadata model_metadata = session->GetModelMetadata();
  cout<<"Model version" << endl;
  cout << model_metadata.GetVersion()<<endl;
  cout<<"Model producer" << endl;
  cout << model_metadata.GetProducerNameAllocated(allocator).get()<<endl;
  cout<<"Model description" << endl;
  cout << model_metadata.GetDescriptionAllocated(allocator).get()<<endl; 
  auto model_meta_data_keys = model_metadata.GetCustomMetadataMapKeysAllocated(allocator);
  cout << "Number of custom metadata keys" << endl;
  cout << model_meta_data_keys.size() << endl;
  cout << "Custom metadata keys" << endl;
  for(int key = 0; key < model_meta_data_keys.size(); key++){
  cout << model_meta_data_keys[key].get() << endl;
  cout << model_metadata.LookupCustomMetadataMapAllocated(model_meta_data_keys[key].get(),allocator).get() << endl;
  if(strcmp(model_meta_data_keys[key].get(), "maxWeight") == 0){
    //cout << "Overwriting maxWeight" << endl;
    loggerPtrIn->INFO_MSG("Overwriting maxWeight from Settings with ONNX metadata");
    maxWeight = std::stod(model_metadata.LookupCustomMetadataMapAllocated(model_meta_data_keys[key].get(),allocator).get());
    //cout << maxWeight << endl;
  }
  }

  /**************** Input info ******************/
  // Get the number of input nodes

  size_t numInputNodes = session->GetInputCount();
  cout << "******* Model information below *******" << std::endl;
  cout << "Number of Input Nodes: " << numInputNodes << std::endl;

	inputName = std::move(session->GetInputNameAllocated(0, allocator));
	inputNodeNames.push_back(inputName.get());
  cout << "Name of Input Nodes: " << endl;
  for(int i = 0; i < numInputNodes; i++){
    cout << inputNodeNames[i] << endl;
  }
  //cout << inputNodeNames[0] << endl;
  // Get the type of the input
  // 0 means the first input of the model
  Ort::TypeInfo inputTypeInfo = session->GetInputTypeInfo(0);
  auto inputTensorInfo = inputTypeInfo.GetTensorTypeAndShapeInfo();
  // Get the shape of the input
  vector<int64_t> mInputDims = inputTensorInfo.GetShape();
  cout << "Input Dimensions: " <<mInputDims[0] <<"\t" << mInputDims[1] << endl;
  mInputDims[0] = 1;  
  int total_number_elements = mInputDims[0]*mInputDims[1];

  /**************** Output info ******************/

  // Get the number of output nodes
  size_t numOutputNodes = session->GetOutputCount();
  cout << "******* Model information below *******" << endl;
  cout << "Number of Output Nodes: " << numOutputNodes << endl;

	outputName = std::move(session->GetOutputNameAllocated(0, allocator));
	outputNodeNames.push_back(outputName.get());
  cout << "Name of Output Nodes: " << endl;
  for(int i = 0; i < numOutputNodes; i++){
    cout << outputNodeNames[i] << endl;
  }
  // Get the type of the output
  // 0 means the first output of the model
  Ort::TypeInfo outputTypeInfo = session->GetOutputTypeInfo(0);
  auto outputTensorInfo = outputTypeInfo.GetTensorTypeAndShapeInfo();
  
  // Get the shape of the input
  vector<int64_t> mOutputDims = outputTensorInfo.GetShape();
  cout << "Output Dimensions: " <<mOutputDims[0] <<"\t" << mOutputDims[1] << endl;
  mOutputDims[0] = 1;
  cout << "Output Dimensions: " << mOutputDims[1] << endl;


  int outputTensorSize = mOutputDims[0]*mOutputDims[1];

  Ort::MemoryInfo memoryInfo = Ort::MemoryInfo::CreateCpu(
      OrtAllocatorType::OrtArenaAllocator, OrtMemType::OrtMemTypeDefault);

  // Set the input tensors.
  inVals.resize(mInputDims[1]);
  input.push_back(Ort::Value::CreateTensor<float>(
      memoryInfo, inVals.data(), total_number_elements, mInputDims.data(),
      mInputDims.size()));

  //cout << "Initial value of input 0" << endl;
  //cout << inVals[0] << endl;
  //cout << input.front().GetTensorMutableData<float>()[0] << endl;

  // Set the output tensors.
  outVals.resize(mOutputDims[1]);
  output.push_back(Ort::Value::CreateTensor<float>(
      memoryInfo, outVals.data(), outputTensorSize,
      mOutputDims.data(), mOutputDims.size()));

  // This is how you access the result.
  //cout << "Initialization log weight" << endl;
  //cout << outVals[0] << "\n";
  //------------------------------------------------------------------------
  
  // This live in the method now.
  session->Run(Ort::RunOptions{nullptr}, inputNodeNames.data(),
    input.data(), input.size(), outputNodeNames.data(), output.data(), output.size());

  // This is how you access the result.
  //cout << "log weight after session Run" << endl;
  //cout << outVals[0] << "\n";

  // run initCheck
  OnnxUser::initCheck(loggerPtrIn);
  
}

//--------------------------------------------------------------------------
bool OnnxUser::canChangeFragPar(){
  //cout <<"Calling canChangeFragPar"<<endl;
  //cout << MyHadronizationModelUH::canChangeFragParFlag << endl;
  return canChangeFragParFlag;
}

//--------------------------------------------------------------------------
bool OnnxUser::initCheck(Logger* loggerPtrIn){
  // add here checks that things work, application dependent
  if(inVals.size()!=7){
    loggerPtrIn->ABORT_MSG("WRONG INPUT SHAPE");
  }
  if(outVals.size()!=1){
    loggerPtrIn->ABORT_MSG("WRONG OUTPUT SHAPE");
  }
  return true;
}
//--------------------------------------------------------------------------
bool OnnxUser::doVetoFragmentation(Particle had, const StringEnd* end){
  //cout <<"Calling doVetoFragmentation"<<endl;
  //cout << end->zHad<<endl;
  double eff = 1.0;
  eff = FragmentationWeight(end)/maxWeight;
  //cout << "Efficiency " << eff << endl;
  if(eff > 1){
    cout << "Efficiency is too big, increase maxWeight" << endl;
    cout << eff*maxWeight << "\t" << maxWeight << endl;
  }
  // random number to decide to veto or not
  double rand = rndmPtr->flat();
  if(rand < eff){
  //cout<<"Accepted"<<endl;
  return false;
  }
  else{
  //cout<<"Rejected"<<endl;
  return true;
  }
}
//--------------------------------------------------------------------------
double OnnxUser::FragmentationWeight(const StringEnd* end){
 
 inVals[0]=(float)end->zHad;
 inVals[1]=(float)end->pxNew;
 inVals[2]=(float)end->pyNew;
 inVals[3]=(float)(end->mHad/0.140);
 inVals[4]=(end->fromPos == 1) ? (float)1 : (float)-1;
 inVals[5]=(float)end->pxOld;
 inVals[6]=(float)end->pyOld;
 // This live in the method now.
 session->Run(Ort::RunOptions{nullptr}, inputNodeNames.data(),
  input.data(), input.size(), outputNodeNames.data(), output.data(), output.size()); 

 //cout << "zHad "<< inVals[0] << "\n";
 //cout << "fromPos "<< inVals[4] << "\n";
 //cout << "log weight " << outVals[0] << "\n";
 return exp((double)outVals[0]);
}

//==========================================================================

} // end namespace ExampleML

} // end namespace Pythia8Contrib

} // end namespace Pythia8
