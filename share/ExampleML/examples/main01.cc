// main01.cc is a part of the PYTHIA-CONTRIB package MyHadronizationModel adapting main06.
// Copyright (C) 2023 AUTHORS.
// MyHadronizationModel is licenced under the GNU GPL v2 or later, see COPYING for details.
// Please respect the MCnet Guidelines, see GUIDELINES for details.

// Keywords: basic usage

// This is a simple test program for the plugin classes of
// MyHadronizationModel. Any number of command files can be passed as command line
// arguments.

// Copyright (C) 2024 Torbjorn Sjostrand.
// PYTHIA is licenced under the GNU GPL v2 or later, see COPYING for details.
// Please respect the MCnet Guidelines, see GUIDELINES for details.

// Keywords: basic usage; electron-positron; event shapes; jet finding

// This is a simple test program.
// It studies event properties of LEP1 events.

#include "Pythia8/Pythia.h"

using namespace Pythia8;

int main() {

  // Generator.
  Pythia pythia;
  pythia.readString("print:quiet = off");  

  // nominal hadronization parameters
  pythia.readString("StringPT:sigma=0.335");
  pythia.readString("StringZ:aLund = 0.68");
  pythia.readString("StringZ:bLund = 0.98");
  pythia.readString("StringZ:aExtraSQuark=0.0");
  pythia.readString("StringZ:aExtraDiquark=0.0");
  pythia.readString("StringZ:rFactC=0.0");
  pythia.readString("StringZ:rFactB=0.0");

  pythia.readString("VariationFrag:List = {a=0.3 frag:aLund=0.3}");
  // Allow no substructure in e+- beams: normal for corrected LEP data.
  pythia.readString("PDF:lepton = off");
  // Process selection.
  pythia.readString("WeakSingleBoson:ffbar2gmZ = on");
  // Switch off all Z0 decays and then switch back on those to up quarks.
  pythia.readString("23:onMode = off");
  pythia.readString("23:onIfAny = 2");
  pythia.readString("Random:setSeed = true");
  pythia.readString("Random:seed = 42");

  // Only pions
  pythia.readString("111:mayDecay = false");
  pythia.readString("211:mayDecay = false");
  pythia.readString("TimeShower:nGluonToQuark = 0");
  pythia.readString("TimeShower:nGammaToQuark = 0");
  pythia.readString("TimeShower:nGammaToLepton = 0");
  pythia.readString("TimeShower:QEDshowerByQ = off");
  pythia.readString("TimeShower:QEDshowerByL = off");
  pythia.readString("TimeShower:QEDshowerByOther = off");
  pythia.readString("TimeShower:QEDshowerByGamma = off");
  pythia.readString("113:m0 = 100.");
  pythia.readString("115:m0 = 100.");
  pythia.readString("213:m0 = 100.");
  pythia.readString("215:m0 = 100.");
  pythia.readString("StringFlav:probStoUD=0.");
  pythia.readString("StringFlav:probQQtoQ = 0.0");
  pythia.readString("StringFlav:mesonUDvector=0.");
  pythia.readString("StringFlav:etaSup=0.0");
  pythia.readString("StringFlav:etaPrimeSup=0.0");

  // LEP1 initialization at Z0 mass.
  pythia.readString("Beams:idA =  11");
  pythia.readString("Beams:idB = -11");
  double mZ = pythia.particleData.m0(23);
  pythia.settings.parm("Beams:eCM", mZ);
  pythia.init();

  // Histograms.
  Hist nCharge("charged multiplicity", 25, 1, 51);
  Hist nChargeVar("charged multiplicity", 25, 1, 51);
  Hist nChargeNN("charged multiplicity", 25, 1, 51);
  

  // Begin event loop. Generate event. Skip if error. List first few.
  for (int iEvent = 0; iEvent < 10000; ++iEvent) {
    if (!pythia.next()) continue;

    // Find and histogram charged multiplicity.
    int nCh = 0;
    for (int i = 0; i < pythia.event.size(); ++i)
      if (pythia.event[i].isFinal() && pythia.event[i].isCharged()) ++nCh;
    nCharge.fill( nCh );
    nChargeVar.fill( nCh,pythia.info.getGroupWeight(0));

  // End of event loop. Statistics. Output histograms.
  }

  // Generator.
  Pythia pythia2;
  pythia2.readString("print:quiet = off");
  // Load Plugin
  pythia2.readString("Init:plugins = {libExampleML.so::OnnxUser}");
  pythia2.readString("OnnxUser:canChangeFragPar = on");
  pythia2.readString("OnnxUser:hadronizationNN = ../models/new_model_with_metadata.onnx");
  pythia2.readString("OnnxUser:maxWeight = 7");
  

  // nominal hadronization parameters
  pythia2.readString("StringPT:sigma=0.335");
  pythia2.readString("StringZ:aLund = 0.68");
  pythia2.readString("StringZ:bLund = 0.98");
  pythia2.readString("StringZ:aExtraSQuark=0.0");
  pythia2.readString("StringZ:aExtraDiquark=0.0");
  pythia2.readString("StringZ:rFactC=0.0");
  pythia2.readString("StringZ:rFactB=0.0");

  // Allow no substructure in e+- beams: normal for corrected LEP data.
  pythia2.readString("PDF:lepton = off");
  // Process selection.
  pythia2.readString("WeakSingleBoson:ffbar2gmZ = on");
  // Switch off all Z0 decays and then switch back on those to up quarks.
  pythia2.readString("23:onMode = off");
  pythia2.readString("23:onIfAny = 2");
  pythia2.readString("Random:setSeed = true");
  pythia2.readString("Random:seed = 42");

  // Only pions
  pythia2.readString("111:mayDecay = false");
  pythia2.readString("211:mayDecay = false");
  pythia2.readString("TimeShower:nGluonToQuark = 0");
  pythia2.readString("TimeShower:nGammaToQuark = 0");
  pythia2.readString("TimeShower:nGammaToLepton = 0");
  pythia2.readString("TimeShower:QEDshowerByQ = off");
  pythia2.readString("TimeShower:QEDshowerByL = off");
  pythia2.readString("TimeShower:QEDshowerByOther = off");
  pythia2.readString("TimeShower:QEDshowerByGamma = off");
  pythia2.readString("113:m0 = 100.");
  pythia2.readString("115:m0 = 100.");
  pythia2.readString("213:m0 = 100.");
  pythia2.readString("215:m0 = 100.");
  pythia2.readString("StringFlav:probStoUD=0.");
  pythia2.readString("StringFlav:probQQtoQ = 0.0");
  pythia2.readString("StringFlav:mesonUDvector=0.");
  pythia2.readString("StringFlav:etaSup=0.0");
  pythia2.readString("StringFlav:etaPrimeSup=0.0");

  // LEP1 initialization at Z0 mass.
  pythia2.readString("Beams:idA =  11");
  pythia2.readString("Beams:idB = -11");
  pythia2.settings.parm("Beams:eCM", mZ);
  pythia2.init();


  //pythia.readString("OnnxUser:canChangeFragPar = on");
  //pythia.readString("Random:setSeed = true");
  //pythia.readString("Random:seed = 20");
  //pythia.init();
  // Begin event loop. Generate event. Skip if error. List first few.
  for (int iEvent = 0; iEvent < 10000; ++iEvent) {
    if (!pythia2.next()) continue;

    // Find and histogram charged multiplicity.
    int nCh = 0;
    for (int i = 0; i < pythia2.event.size(); ++i)
      if (pythia2.event[i].isFinal() && pythia2.event[i].isCharged()) ++nCh;
    nChargeNN.fill( nCh );


  // End of event loop. Statistics. Output histograms.
  }
  pythia2.stat();
  cout << nCharge << nChargeVar << nChargeNN;

  // Create the Python plot and return.
  HistPlot hpl("main01plot");
  hpl.frame("main01plot", "Charged Multiciplicity Comparison", "charged multiplicity", "PDF");

  hpl.add(nCharge, "e", "Nominal 0.68");
  hpl.add(nChargeVar, "e", "Variation 0.30");
  hpl.add(nChargeNN, "e", "NN");
  hpl.plot();
  // Done.
  return 0;
}

