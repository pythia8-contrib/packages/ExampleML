// OnnxUser.h is a part of the PYTHIA-CONTRIB package ExampleML.
// Copyright (C) 2023 AUTHORS.
// ExampleML is licenced under the GNU GPL v2 or later, see COPYING for details.
// Please respect the MCnet Guidelines, see GUIDELINES for details.

#ifndef ExampleML_OnnxUser_H
#define ExampleML_OnnxUser_H

#include "Pythia8/Pythia.h"
#include "Pythia8/Plugins.h"
#include <onnxruntime/onnxruntime_cxx_api.h>

namespace Pythia8 {

namespace Pythia8Contrib {

namespace ExampleML {

//==========================================================================

// Add documentation about the class OnnxUser here.
// The following template declares all methods that can be inherited from
// UserHooks.

// The base implementation is provided as a comment after the declaration.
// If no comment follows the declaration, then the method is defined in
// src/OnnxUser.cc.

// Methods that are not commented out must be defined, while those
// that are commented out are optional. The declarations and definitions
// of all methods that will not be used should be deleted. The methods
// that will be used should be defined either here, or in 
// src/OnnxUser.cc.

// By convention, define simple methods in this header and more
// involved methods in the corresponding source file. To create
// minimal template rather than this full template, call use the
// option --minimal when using ./generate.

class OnnxUser : public UserHooks {

public:

  // Constructor (defined in src/OnnxUser.cc).
  OnnxUser(Pythia*, Settings*, Logger*);
  
  // Destructor.
  ~OnnxUser() = default;


  // Settings
  bool canChangeFragParFlag;
  string hadronizationNN;
  double maxWeight;

  // checker
  bool initCheck(Logger*);

  // ORT session.
  shared_ptr<Ort::Session> session;
  vector<float> inVals;
  vector<float> outVals;
  vector<Ort::Value> input;
  vector<Ort::Value> output;
  shared_ptr<char> inputName, outputName;
	vector<char*> inputNodeNames;
	vector<char*> outputNodeNames;

  // fragmentation weight from ORT session
  double FragmentationWeight(const StringEnd* end);


  // Can change fragmentation parameters.
  bool canChangeFragPar() override;
  // {return false;}

  // Do a veto on a hadron just before it is added to the final state.
  // The StringEnd from which the the hadron was produced is included
  // for information.
  bool doVetoFragmentation(Particle had, const StringEnd* end) override;
  // {return false;}


};

//==========================================================================

} // end namespace ExampleML

} // end namespace Pythia8Conrib

} // end namespace Pythia8

#endif // ExampleML_OnnxUser_H
